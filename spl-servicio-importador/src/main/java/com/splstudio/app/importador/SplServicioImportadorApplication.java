package com.splstudio.app.importador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SplServicioImportadorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SplServicioImportadorApplication.class, args);
	}

}
